var express = require('express');
var app = express();
var opn = require('opn');

function helloWorld() {
  
}

app.get('/', function (req, res) {
    
    var result = helloWorld();

    if (result) {
      res.send(result);  
    }
    else {
      res.send("I donno what to say.")
    }
});

// IGNORE EVERYTHING BELOW THIS

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');

  opn('http://localhost:3000');
  setTimeout(function() {
    process.exit();
  },3000)
  
});

