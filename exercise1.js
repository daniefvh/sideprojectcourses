var express = require('express');
var app = express();
var opn = require('opn');
app.get('/', function (req, res) {

    var testVariable = 5;
    if (testVariable === 1) {
        res.send('SUCCESS!');
    }
    else{
        res.send('OOPS TRY AGAIN!')
    }
});
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');

  opn('http://localhost:3000');
  setTimeout(function() {
    process.exit();
  },10000)
  
});

